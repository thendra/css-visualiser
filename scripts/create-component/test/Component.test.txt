import React from 'react';
import { render } from 'react-testing-library';

import $CNAME from '../$CNAME';

describe('$CNAME Component', () => {
  describe('Renders $CNAME Element', () => {
    it('should render the component', () => {
      const { container } = render(<$CNAME />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
