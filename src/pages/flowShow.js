import React from 'react'

import Layout from '../components/layout'
import SEO from '../components/seo'
import {Box} from '@material-ui/core'
import ControlPanel from '../components/ControlPanel'

const FlowShow = () => {

  return(
    <Layout>
      <SEO title="Home" />
      <Box px={3}>
          <h1>Flow Show</h1>
          <p>This is a tool for visualising how css properties affect how elements behave in the DOM</p>
          <p>Click on the container to alternate it between <Box display='inline' px={1} color='white' bgcolor='#5F5AA2'>Flex</Box> and <Box px={1} display='inline' bgcolor='#70C1B3'>Block</Box> display properties</p>
          <p>Click on the boxes to alternate each one between <strong>Inline Block</strong>, <strong>Block</strong> and <strong>Inline</strong> display properties</p>
          <p>Use the bar on the left to apply properties to the boxes and flex properties to the container</p>
          <ControlPanel />
      </Box>
    </Layout>
  )
}

export default FlowShow
