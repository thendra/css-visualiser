import React from 'react'

import Layout from '../components/layout'
import SEO from '../components/seo'
import { Box, Button, Card, CardContent, Typography } from '@material-ui/core';

const IndexPage = () => {
  return (
    <Layout>
      <SEO title="Home" />
      <h1>Welcome to the CSS Explorer</h1>
      <Box px={3}>
        <Box maxWidth={800}>Here you can browse through various interactive tools that
          can teach you all about the wonders of CSS and how it's not the evil monster you may initially think...</Box>
        <Box display='flex' pt={4}>
          <Box maxWidth='300px'>
            <Card>
              <CardContent>
                <Typography align='center'>Checkout this flow visualiser</Typography>
                <Box display='flex' pt={3} justifyContent='center'>
                  <Button variant='contained' color='primary' href='/flowShow'>Flow Show</Button>
                </Box>
              </CardContent>
            </Card>
          </Box>
        </Box>
      </Box>
    </Layout>
  )
}

export default IndexPage
