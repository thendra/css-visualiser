import React from 'react';

import { storiesOf } from '@storybook/react';

import {
  withKnobs
} from '@storybook/addon-knobs';
import ControlPanel from '../components/ControlPanel';

// Knobs for React props
storiesOf('ControlPanel', module)
  .addDecorator(withKnobs)
  .add('ControlPanel', () => (
    <ControlPanel />
  ));
