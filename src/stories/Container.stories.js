import React from 'react';

import { storiesOf } from '@storybook/react';

import {
  withKnobs
} from '@storybook/addon-knobs';
import Container from '../components/Container';

// Knobs for React props
storiesOf('Container', module)
  .addDecorator(withKnobs)
  .add('Container', () => (
    <Container itemCount={4}/>
  ));
