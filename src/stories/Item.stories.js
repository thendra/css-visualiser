import React from 'react';

import { storiesOf } from '@storybook/react';

import {
  withKnobs
} from '@storybook/addon-knobs';
import Item from '../components/Item';

// Knobs for React props
storiesOf('Item', module)
  .addDecorator(withKnobs)
  .add('Item', () => (
    <Item display={2}/>
  ));
