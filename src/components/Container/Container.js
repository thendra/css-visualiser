import React, { useMemo, useState } from 'react'
import { Box } from '@material-ui/core'
import Item from '../Item'

const Container = ({ itemCount, boxProps, containerProps, flexProps }) => {
  const [displayValue, setDisplayValue] = useState(1)

  const {
    padding,
    margin,
    displayAll,
    includeText,
    boxPosition,
    top,
    right,
    bottom,
    left,
    fixedSize,
  } = boxProps

  const items = useMemo(
    () =>
      Array(itemCount)
        .fill('')
        .map(n => {
          return (
            <Item
              padding={padding}
              top={top}
              right={right}
              bottom={bottom}
              left={left}
              position={boxPosition}
              includeText={includeText}
              margin={margin}
              display={displayAll}
              fixedSize={fixedSize}
            />
          )
        }),
    [itemCount, boxProps]
  )

  const displayKeys = {
    1: { display: 'block', backgroundColor: '#70C1B3' },
    2: { display: 'flex', backgroundColor: '#5F5AA2' },
  }

  const handleClick = () => {
    if (displayValue === 1) {
      setDisplayValue(2)
    } else {
      setDisplayValue(1)
    }
  }

  const { fixedHeight, containerPosition } = containerProps
  const {
    justifyContent,
    alignItems,
    alignContent,
    flexDirection,
    flexWrap,
  } = flexProps

  return (
    <Box
      justifyContent={justifyContent}
      flexWrap={flexWrap}
      alignItems={alignItems}
      alignContent={alignContent}
      flexDirection={flexDirection}
      onClick={() => handleClick()}
      border={1}
      bgcolor={displayKeys[displayValue].backgroundColor}
      display={displayKeys[displayValue].display}
      maxWidth={'1200px'}
      height={fixedHeight && '50vw'}
      minWidth={100}
      width="100%"
      m="auto"
      p={4}
      position={containerPosition}
    >
      {items}
    </Box>
  )
}

Container.propTypes = {}

Container.defaultProps = {
  boxProps: {
    displayAll: 1,
    boxPosition: 'static',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  flexProps: {
    justifyContent: 'center',
    flexWrap: 'nowrap',
    alignItems: 'start',
    alignContent: 'space-between',
    flexDirection: 'row',
  },
  containerProps: {
    fixedHeight: false,
    containerPosition: 'static',
  },
}

export default Container
