import React from 'react';
import { render } from 'react-testing-library';

import Container from '../Container';

describe('Container Component', () => {
  describe('Renders Container Element', () => {
    it('should render the component', () => {
      const { container } = render(<Container />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
