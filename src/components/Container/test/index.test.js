import React from 'react';
import { shallow } from 'enzyme';
import Container from '../Container';
import ContainerInterface from '../index';

jest.mock('../Container', () => ({
  __esModule: true,
  default: jest.fn(),
}));

describe('Container Component interface', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  afterAll(() => {
    jest.restoreAllMocks();
  });
  it('Should export the main component', () => {
    shallow(<Container />);
    expect(ContainerInterface).toHaveBeenCalled();
  });
});