import React from 'react';
import { render } from 'react-testing-library';

import Item from '../Item';

describe('Item Component', () => {
  describe('Renders Item Element', () => {
    it('should render the component', () => {
      const { container } = render(<Item />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
