import React from 'react';
import { shallow } from 'enzyme';
import Box from '../Box';
import BoxInterface from '../index';

jest.mock('../Box', () => ({
  __esModule: true,
  default: jest.fn(),
}));

describe('Box Component interface', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  afterAll(() => {
    jest.restoreAllMocks();
  });
  it('Should export the main component', () => {
    shallow(<Box />);
    expect(BoxInterface).toHaveBeenCalled();
  });
});