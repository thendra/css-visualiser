import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Box} from '@material-ui/core';

const useStyles = makeStyles({
  itemContainer: {
    boxSizing: "content-box",
  }
});

const Item = ({padding, margin, display, includeText, position, top, right, bottom, left, fixedSize}) => {
  const [displayValue, setDisplayValue] =  useState(display);
  const props = {displayValue: displayValue}

  const classes = useStyles(props)
  useEffect(() => {
    setDisplayValue(display);
  }, [display])

  const displayKeys = {
    1: {display:'inline', backgroundColor: '#fb9224'}, 
    2: {display:'inline-block', backgroundColor: '#FFE066'}, 
    3: {display: 'block', backgroundColor: '#F25F5C'}
  };

  const handleClick = (event) => {
    event.stopPropagation();
    if(displayValue < 3) {
      setDisplayValue(displayValue + 1)
    } else {
      setDisplayValue(1)
    }
  }

  return <Box 
          className={classes.itemContainer} 
          bgcolor={displayKeys[displayValue].backgroundColor} 
          onClick={(event) => handleClick(event)} 
          border={1} 
          display={displayKeys[displayValue].display}
          padding={`${padding}px`}
          margin={`${margin}px`}
          position={position}
          top={top}
          right={right}
          bottom={bottom}
          left={left}
          width={fixedSize ? '100px' : undefined}
          height={fixedSize ? '100px' : undefined}
          >{includeText && "Hey I'm some text look at me"}
          
    </Box>
}

Item.propTypes = {
};

Item.defaultProps = {
  display: 2
};

export default Item;
