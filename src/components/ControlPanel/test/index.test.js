import React from 'react';
import { shallow } from 'enzyme';
import ControlPanel from '../ControlPanel';
import ControlPanelInterface from '../index';

jest.mock('../ControlPanel', () => ({
  __esModule: true,
  default: jest.fn(),
}));

describe('ControlPanel Component interface', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  afterAll(() => {
    jest.restoreAllMocks();
  });
  it('Should export the main component', () => {
    shallow(<ControlPanel />);
    expect(ControlPanelInterface).toHaveBeenCalled();
  });
});