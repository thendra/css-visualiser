import React from 'react';
import { render } from 'react-testing-library';

import ControlPanel from '../ControlPanel';

describe('ControlPanel Component', () => {
  describe('Renders ControlPanel Element', () => {
    it('should render the component', () => {
      const { container } = render(<ControlPanel />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
