import React, { useState } from 'react'
import Container from '../Container'
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Typography,
  Box,
  Card,
  CardContent,
  FormControlLabel,
  Switch,
  Button,
  Slider,
  Grid,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  addBtn: {
    backgroundColor: '#24defb',
    '&:hover': {
      backgroundColor: '#24defb',
    },
  },
  remBtn: {
    backgroundColor: '#f078e0',
    '&:hover': {
      backgroundColor: '#f078e0',
    },
  },
  dinlineBtn: {
    backgroundColor: '#fb9224',
    '&:hover': {
      backgroundColor: '#fb9224',
    },
  },
  dblockBtn: {
    backgroundColor: '#F25F5C',
    marginTop: '5px',
    marginBottom: '5px',
    '&:hover': {
      backgroundColor: '#F25F5C',
    },
  },
  dinlineBlockBtn: {
    backgroundColor: '#FFE066',
    '&:hover': {
      backgroundColor: '#FFE066',
    },
  },
  controlPanel: {
    minWidth: 275,
    maxWidth: 400,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
    marginBottom: 10,
  },
})

const ControlPanel = () => {
  const [itemCount, setItemCount] = useState(2)

  const [boxProps, setBoxProps] = useState({
    displayAll: 1,
    boxPadding: 0,
    includeText: true,
    boxPosition: 'static',
    fixedSize: false,
  })
  const [containerProps, setContainerProps] = useState({
    fixedHeight: false,
    containerPosition: 'static',
  })
  const [flexProps, setFlexProps] = useState({
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'nowrap',
  })

  const handleJC = event =>
    setFlexProps({ ...flexProps, justifyContent: event.target.value })
  const handleAI = event =>
    setFlexProps({ ...flexProps, alignItems: event.target.value })
  const handleAC = event =>
    setFlexProps({ ...flexProps, alignContent: event.target.value })
  const handleFD = event =>
    setFlexProps({ ...flexProps, flexDirection: event.target.value })
  const handleFW = event =>
    setFlexProps({ ...flexProps, flexWrap: event.target.value })
  const handleFixedH = event =>
    setContainerProps({ ...containerProps, fixedHeight: event.target.checked })
  const handlePadding = (event, value) =>
    setBoxProps({ ...boxProps, padding: value })
  const handleMargin = (event, value) =>
    setBoxProps({ ...boxProps, margin: value })
  const handleTop = (event, value) => setBoxProps({ ...boxProps, top: value })
  const handleRight = (event, value) =>
    setBoxProps({ ...boxProps, right: value })
  const handleBottom = (event, value) =>
    setBoxProps({ ...boxProps, bottom: value })
  const handleLeft = (event, value) => setBoxProps({ ...boxProps, left: value })
  const handleIncludeText = event =>
    setBoxProps({ ...boxProps, includeText: event.target.checked })
  const handleBoxPosition = event =>
    setBoxProps({ ...boxProps, boxPosition: event.target.value })
  const handleFixedSize = event =>
    setBoxProps({ ...boxProps, fixedSize: event.target.checked })
  const handleContainerPosition = event =>
    setContainerProps({
      ...containerProps,
      containerPosition: event.target.value,
    })
  // const handleBoxSizing = (event) => setContainerProps({...boxProps, boxSizing: event.target.value});

  const valuetext = value => `${value}px`
  const classes = useStyles()
  return (
    <Grid container>
      <Grid item md={6}>
        <Box display="flex" flexWrap="wrap" justifyContent="space-between">
          <Card className={classes.controlPanel}>
            <CardContent>
              <Typography variant="h5">Box Properties</Typography>
              <Box display="flex" justifyContent="space-around">
                <Button
                  className={classes.addBtn}
                  variant="contained"
                  onClick={() => itemCount < 50 && setItemCount(itemCount + 1)}
                >
                  + Add Box{' '}
                </Button>
                <Button
                  className={classes.remBtn}
                  variant="contained"
                  onClick={() => itemCount > 0 && setItemCount(itemCount - 1)}
                >
                  - Remove Box{' '}
                </Button>
              </Box>
              <Box py={2}>
                <Button
                  className={classes.dinlineBtn}
                  fullWidth
                  variant="contained"
                  disabled={boxProps.displayAll === 1}
                  onClick={() => setBoxProps({ ...boxProps, displayAll: 1 })}
                >
                  display all inline
                </Button>
                <Button
                  className={classes.dblockBtn}
                  fullWidth
                  variant="contained"
                  disabled={boxProps.displayAll === 3}
                  onClick={() => setBoxProps({ ...boxProps, displayAll: 3 })}
                >
                  display all block
                </Button>
                <Button
                  className={classes.dinlineBlockBtn}
                  fullWidth
                  variant="contained"
                  disabled={boxProps.displayAll === 2}
                  onClick={() => setBoxProps({ ...boxProps, displayAll: 2 })}
                >
                  display all inline block
                </Button>
              </Box>
              <Box>
                <Typography>Box Padding</Typography>
                <Slider
                  onChange={handlePadding}
                  defaultValue={0}
                  getAriaValueText={valuetext}
                  aria-labelledby="discrete-slider"
                  valueLabelDisplay="auto"
                  step={1}
                  min={0}
                  max={50}
                />
              </Box>
              <Box>
                <Typography>Box Margin</Typography>
                <Slider
                  onChange={handleMargin}
                  defaultValue={0}
                  getAriaValueText={valuetext}
                  aria-labelledby="discrete-slider"
                  valueLabelDisplay="auto"
                  step={1}
                  min={0}
                  max={50}
                />
              </Box>
              <FormControlLabel
                control={
                  <Switch
                    checked={boxProps.includeText}
                    onChange={handleIncludeText}
                    value="includeText"
                  />
                }
                label="Include Text"
              />
              <FormControlLabel
                control={
                  <Switch
                    checked={boxProps.fixedSize}
                    onChange={handleFixedSize}
                    value="fixedSize"
                  />
                }
                label="Fixed Size"
              />
              <FormControl fullWidth margin="normal">
                <InputLabel id="demo-simple-select-label">
                  Box Position
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={boxProps.boxPosition}
                  onChange={handleBoxPosition}
                >
                  <MenuItem value={'static'}>Static</MenuItem>
                  <MenuItem value={'relative'}>Relative</MenuItem>
                  <MenuItem value={'absolute'}>Absolute</MenuItem>
                  <MenuItem value={'fixed'}>Fixed</MenuItem>
                </Select>
              </FormControl>
              <Box display="flex" justifyContent="space-between">
                <Box width="150px">
                  <Typography>Box Top</Typography>
                  <Slider
                    onChange={handleTop}
                    defaultValue={0}
                    getAriaValueText={valuetext}
                    aria-labelledby="discrete-slider"
                    valueLabelDisplay="auto"
                    step={10}
                    min={0}
                    max={500}
                  />
                </Box>
                <Box width="150px">
                  <Typography>Box Right</Typography>
                  <Slider
                    onChange={handleRight}
                    defaultValue={0}
                    getAriaValueText={valuetext}
                    aria-labelledby="discrete-slider"
                    valueLabelDisplay="auto"
                    step={10}
                    min={0}
                    max={500}
                  />
                </Box>
              </Box>
              <Box display="flex" justifyContent="space-between">
                <Box width="150px">
                  <Typography>Box Bottom</Typography>
                  <Slider
                    onChange={handleBottom}
                    defaultValue={0}
                    getAriaValueText={valuetext}
                    aria-labelledby="discrete-slider"
                    valueLabelDisplay="auto"
                    step={10}
                    min={0}
                    max={500}
                  />
                </Box>
                <Box width="150px">
                  <Typography>Box Left</Typography>
                  <Slider
                    onChange={handleLeft}
                    defaultValue={0}
                    getAriaValueText={valuetext}
                    aria-labelledby="discrete-slider"
                    valueLabelDisplay="auto"
                    step={10}
                    min={0}
                    max={500}
                  />
                </Box>
              </Box>
            </CardContent>
          </Card>
          <Card component={Box} width="300px">
            <CardContent>
              <Typography variant="h5">Container Properties</Typography>
              <FormControl fullWidth margin="normal">
                <InputLabel id="demo-simple-select-label">
                  Container Position
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={containerProps.containerPosition}
                  onChange={handleContainerPosition}
                >
                  <MenuItem value={'static'}>Static</MenuItem>
                  <MenuItem value={'relative'}>Relative</MenuItem>
                  <MenuItem value={'absolute'}>Absolute</MenuItem>
                  <MenuItem value={'sticky'}>Sticky</MenuItem>
                </Select>
              </FormControl>
              <FormControlLabel
                control={
                  <Switch
                    checked={containerProps.fixedHeight}
                    onChange={handleFixedH}
                    value="fixedHeight"
                  />
                }
                label="Fixed Height"
              />
              <Box pt={4}>
                <Typography>Flex Properties</Typography>
                <FormControl fullWidth margin="normal">
                  <InputLabel id="demo-simple-select-label">
                    Justify Content
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={flexProps.justifyContent}
                    onChange={handleJC}
                  >
                    <MenuItem value={'center'}>Center</MenuItem>
                    <MenuItem value={'space-between'}>Space between</MenuItem>
                    <MenuItem value={'space-evenly'}>Space evenly</MenuItem>
                    <MenuItem value={'flex-start'}>Flex start</MenuItem>
                    <MenuItem value={'flex-end'}>Flex end</MenuItem>
                  </Select>
                </FormControl>
                <FormControl fullWidth margin="normal">
                  <InputLabel id="demo-simple-select-label">
                    Align Items
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={flexProps.alignItems}
                    onChange={handleAI}
                  >
                    <MenuItem value={'center'}>Center</MenuItem>
                    <MenuItem value={'stretch'}>Stretch</MenuItem>
                    <MenuItem value={'flex-start'}>Flex start</MenuItem>
                    <MenuItem value={'flex-end'}>Flex end</MenuItem>
                  </Select>
                </FormControl>
                <FormControl fullWidth margin="normal">
                  <InputLabel id="demo-simple-select-label">
                    Align Content
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={flexProps.alignContent}
                    onChange={handleAC}
                  >
                    <MenuItem value={'flex-start'}>Flex start</MenuItem>
                    <MenuItem value={'flex-end'}>Flex end</MenuItem>
                    <MenuItem value={'center'}>Center</MenuItem>
                    <MenuItem value={'stretch'}>Stretch</MenuItem>
                    <MenuItem value={'space-between'}>Space between</MenuItem>
                    <MenuItem value={'space-around'}>Space around</MenuItem>
                  </Select>
                </FormControl>
                <FormControl fullWidth margin="normal">
                  <InputLabel id="demo-simple-select-label">
                    Flex Direction
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={flexProps.flexDirection}
                    onChange={handleFD}
                  >
                    <MenuItem value={'row'}>Row</MenuItem>
                    <MenuItem value={'column'}>Column</MenuItem>
                  </Select>
                </FormControl>
                <FormControl fullWidth margin="normal">
                  <InputLabel id="demo-simple-select-label">
                    Flex Wrap
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={flexProps.flexWrap}
                    onChange={handleFW}
                  >
                    <MenuItem value={'wrap'}>Wrap</MenuItem>
                    <MenuItem value={'nowrap'}>No wrap</MenuItem>
                    <MenuItem value={'wrap-reverse'}>Wrap reverse</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </CardContent>
          </Card>
        </Box>
      </Grid>
      <Grid item md={6}>
        <Box padding={4}>
          <Container
            itemCount={itemCount}
            boxProps={boxProps}
            containerProps={containerProps}
            flexProps={flexProps}
          />
        </Box>
      </Grid>
    </Grid>
  )
}

ControlPanel.propTypes = {}

ControlPanel.defaultProps = {}

export default ControlPanel
